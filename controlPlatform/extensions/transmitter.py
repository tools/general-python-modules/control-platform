from ..extension import __Extension__

class Transmitter(__Extension__):
	"""
	Extends a platform with transmitter functions
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Transmitter")
