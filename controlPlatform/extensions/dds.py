from ..extension import __Extension__

class Dds(__Extension__):
	"""
	Extends a platform with "Dds" functionality
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "DDS")

	def configure(self):
		"""
		Signal to the DDS platform that configuration is finished and that DDS can now be programmed 
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "EXTERNAL", "DDS", "FINISHED"]))
