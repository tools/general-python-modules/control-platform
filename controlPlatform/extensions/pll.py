from ..extension import __Extension__


class Pll(__Extension__):
    """
    Extends a platform with "Pll" functionality
    """

    def __init__(self, platform, configHandler):
        super().__init__(self, platform, configHandler, "PLL")

    def configure(self):
        """
        Signal to the PLL platform that configuration is finished and that PLL can now be programmed 
        """
        self.configHandler.communicate(
            self.interface.send(["CONFIG", "EXTERNAL", "PLL", "FINISHED"]))
