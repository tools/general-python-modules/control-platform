from ..extension import __Extension__

class Trigger(__Extension__):
	"""
	Extends a platform with trigger functions
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Trigger")

	def force(self):
		"""
		Forces a trigger impulse in all receivers. This will be asynchronous
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "TRIGGER", "FORCE"]))

	def start(self):
		"""
		Starts the trigger impulses (or burst) on the trigger board (central platform)
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "TRIGGER", "START"]))

	def stop(self):
		"""
		Stops the trigger impulses. Also resets the frequency hopping counter.
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "TRIGGER", "STOP"]))

	def burst(self, burst):
		"""
		Sets the burst count of trigger impulses
		"""
		self.configHandler.communicate(self.interface.sendInt(["CONFIG", "TRIGGER", "BURST"], burst))

	def rate(self, rate):
		"""
		Sets a new trigger frequency on the trigger board
		"""
		self.configHandler.communicate(self.interface.sendDouble(["CONFIG", "TRIGGER", "RATE"], rate))

	def finish(self):
		"""
		Triggers configuration on embedded hardware
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "TRIGGER", "FINISHED"]))

	def configure(self, rate = 1, burst = 1):
		"""
		Configure periodic trigger
		"""
		print("Configure periodic trigger on %s." % self.platform.name)

		self.burst(burst)
		self.rate(rate)
		self.finish()
