from ..extension import __Extension__

class Boot(__Extension__):
	"""
	Extends a platform by the boot mode. It interfaces to the embedded bootloader, if present.
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Boot")

	def flashErase(self):
		"""
		Erase the flash on the connected platform
		"""
		self.configHandler.communicate(self.interface.send(["CONFIG", "FLASH", "ERASE"]))

	def flashWrite(self, data):
		"""
		Erase the flash on the connected platform
		"""
		self.configHandler.communicate(self.interface.sendBinary(["CONFIG", "FLASH", "WRITE"], data))

	def flash(self, data):
		"""
		Flashes the image.

		Parameters:
		-----------
		data : bin
			Binary image for the processor
		"""
		print("Flashing %s." % self.platform.name)
		self.flashErase()
		self.flashWrite(data)
		self.run()

	def run(self):
		"""
		Jumps out of bootloader to run the application.
		"""
		print("Run flash on %s." % self.platform.name)
		self.configHandler.communicate(self.interface.send(["CONFIG", "FLASH", "RESET"]))