from ..extension import __Extension__

class Central(__Extension__):
	"""
	Extends a platform by the "Central" mode
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Central")
