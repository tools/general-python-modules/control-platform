from ..extension import __Extension__


class Fmcw(__Extension__):
    """
    Extends a platform with "Fmcw" functionality
    """

    def __init__(self, platform, configHandler):
        super().__init__(self, platform, configHandler, "FMCW")

    def fmcwConfigBandwidth(self, bandwidth):
        """
        Program chirp bandwidth
        """
        self.configHandler.communicate(self.interface.sendDouble(
            ["CONFIG", "EXTERNAL", "FMCW", "CHIRP_BANDWIDTH"], bandwidth))

    def fmcwConfigFrequency(self, startFreq):
        """
        Program starting frequency of the chirp
        """
        self.configHandler.communicate(
            self.interface.sendDouble(["CONFIG", "EXTERNAL", "FMCW", "CHIRP_BASE_F"], startFreq))

    def fmcwConfigOffsetFrequency(self, offsetFreq):
        """
        Program starting frequency of the chirp
        """
        self.configHandler.communicate(self.interface.sendDouble(
            ["CONFIG", "EXTERNAL", "FMCW", "CHIRP_OFF_F"], offsetFreq))

    def fmcwConfigHopFrequency(self, hopFreq):
        """
        Program hopping frequency between chirps
        """
        self.configHandler.communicate(self.interface.sendDouble(
            ["CONFIG", "EXTERNAL", "FMCW", "CHIRP_HOP_F"], hopFreq))

    def fmcwConfigDuration(self, duration):
        """
        Program duration of the chirp
        """
        self.configHandler.communicate(self.interface.sendDouble(
            ["CONFIG", "EXTERNAL", "FMCW", "CHIRP_DURATION"], duration))

    def fmcwConfigSamplingDelay(self, duration):
        """
        Program delay after which amplifier switches on and starts to transmit (compensation)
        """
        self.configHandler.communicate(self.interface.sendDouble(
            ["CONFIG", "EXTERNAL", "FMCW", "TXDELAY"], duration))

    def configure(self, bw, duration, fc, foffset=0, fhop=0, samplingDelay=0):
        """
        Configures the FMCW settings on the platform

        Parameters:
        """

        print("Configure FMCW on %s." % self.platform.name)

        # Transmit individual configuration settings
        self.fmcwConfigBandwidth(bw)
        self.fmcwConfigFrequency(fc)
        self.fmcwConfigOffsetFrequency(foffset)
        self.fmcwConfigHopFrequency(fhop)
        self.fmcwConfigDuration(duration)
        self.fmcwConfigSamplingDelay(samplingDelay)
