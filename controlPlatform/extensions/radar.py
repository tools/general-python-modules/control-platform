from ..extension import __Extension__

class Radar(__Extension__):
	"""
	Extends a platform with "Radar" functionality
	"""

	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Radar")

	def informRxCount(self, rxCount):
		"""
		Informs the platform about the total count of receivers
		"""
		self.configHandler.communicate(self.interface.sendInt(["CONFIG", "SYS", "EXTERNAL", "SYS_RX_CNT"], rxCount))

	def informTxCount(self, txCount):
		"""
		Informs the platform about the total count of transmitters
		"""
		self.configHandler.communicate(self.interface.sendInt(["CONFIG", "SYS", "EXTERNAL", "SYS_TX_CNT"], txCount))

	def configure(self, rxCnt, txCnt):
		"""
		Configures the Radar settings on the platform
		"""
		print("Configure radar system settings on %s." % self.platform.name)

		# Transmit individual configuration settings
		self.informRxCount(rxCnt)
		self.informTxCount(txCnt)
