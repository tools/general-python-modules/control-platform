from ..extension import __Extension__

class Calibrator(__Extension__):
	"""
	Extends a platform by the "Calibrator" mode
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Calibrator")
