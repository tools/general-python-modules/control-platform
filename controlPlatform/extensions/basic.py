from ..extension import __Extension__


class Basic(__Extension__):
    """
    No mode is yet discovered for a platform with only the Basic extension.
    This extension provides basic functionality, 
    """

    def __init__(self, platform, configHandler, family):
        super().__init__(self, platform, configHandler, "Basic")

        # The family is used for resolving properties of a platform, based on its unique identifier
        self.family = family

        # Unique identifier, e.g. MAC address
        self.uuid = None

        # A color code for this platform
        self.colorCode = None

        # Unique name, e.g. Niko
        self.uname = None

        # Type of this platform
        self.platformType = None

        # Family member. The name and position of this platform within the framework
        self.member = None

    def informIndex(self, index):
        """
        Informs a platform about its position in the array by index. This is mostly
        relevant for transmitters that select a trigger impulse in the burst of triggers by index
        """
        try:
            self.configHandler.communicate(self.interface.sendInt(
                ["CONFIG", "SYS", "IDX"], index))

        except AttributeError:
            # This platform has no index, since it has no position
            pass

    def informRxCount(self, rx_cnt):
        """
        Informs a platform about total number of receivers
        """
        self.configHandler.communicate(self.interface.sendInt(
            ["CONFIG", "SYS", "EXTERNAL", "SYS_RX_CNT"], rx_cnt))

    def informTxCount(self, tx_cnt):
        """
        Informs a platform about total number of receivers
        """
        self.configHandler.communicate(self.interface.sendInt(
            ["CONFIG", "SYS", "EXTERNAL", "SYS_TX_CNT"], tx_cnt))

    def reset(self):
        """
        Sends a soft reset signal to the platform
        """
        print("Reset %s to bootloader." % self.platform.name)
        self.configHandler.communicate(self.interface.send(["REQUEST", "RST"]))

    def scanTest(self):
        """
        Connects to the platform for testing
        """
        return self.configHandler.testconn()

    def getType(self):
        """
        Scans the connected platform for its type, if there is a platform at the specified address
        """
        # Ask platform for its type
        platformType = self.configHandler.communicate(
            self.interface.send(["REQUEST", "TYPE", "NAME"]))

        # hello!
        return platformType.pop()

    def getSecondaryType(self):
        """
        Scans the connected platform for its secondary type, if there is a platform at the specified address
        """
        # Ask platform for its secondary type
        secondaryType = self.configHandler.communicate(
            self.interface.send(["REQUEST", "TYPE", "EXTENSION"]))

        # hello!
        return secondaryType.pop()

    def getId(self):
        """
        Asks for platform unique identifier
        """

        # Extracts the unique id from the network handler
        uuid = self.configHandler.communicate(
            self.interface.send(["REQUEST", "TYPE", "UUID"]), sections=[("uint8", None)])

        # Convert the uuid to a string for display and storage, byte by byte
        uuidStr = '0x'

        for field in uuid:
            uuidStr = '%s%02x' % (uuidStr, field)

        return uuidStr

    def learnName(self):
        """
        This platform now learns its own name and properties and stores it
        """
        self.member = self.family.getMemberByUuid(self.uuid)

        # Store platform position as a top level property for later sorting
        if self.member is not None:
            # Platform is known
            self.platform.pos = self.member.position
            self.platform.name = self.member.name

    def scan(self):
        """
        Scan for this platform on the network
        Return object ID for the task
        """
        self.platform.exists = self.scanTest()

        if self.platform.exists:
            self.identify()

        return self.platform

    def informIndex(self):
        """
        Inform platform of its index
        """
        try:
            self.informIndex(self.platform.idx)
        except:
            # Only inform platforms that have an index.
            pass

    def identify(self):
        """
        Extracts general information from platform, as well as uuid/name
        """
        self.platformType = self.getType()
        self.platformSecondaryType = self.getSecondaryType()
        self.uuid = self.getId()

        self.learnName()
