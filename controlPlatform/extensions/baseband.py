from ..extension import __Extension__

class Baseband(__Extension__):
	"""
	Extends a platform with baseband control functions
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Baseband")

	def setStageLevel(self, level, stage):
		"""
		Sets baseband level for a certain stage

		Parameter:
		----------
		level : double
			The attenuation/gain level in a unit of choice. This depends on the user implementation
		
		stage : int
			The gain stage index, for which the gain is changed
		"""
		self.configHandler.communicate((self.interface.sendDouble(["CONFIG", "EXTERNAL", "BB", stage], level)))

	def setLevels(self, **levels):
		"""
		Sets baseband levels on a platform.
		"""
		print("Set baseband levels on %s." % self.platform.name)

		for stage in levels:
			self.setStageLevel(levels[stage], stage)