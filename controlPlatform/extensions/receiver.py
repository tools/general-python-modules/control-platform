from ..extension import __Extension__

class Receiver(__Extension__):
	"""
	Extends a platform with the "Receiver" mode
	"""
	def __init__(self, platform, configHandler):
		super().__init__(self, platform, configHandler, "Receiver")