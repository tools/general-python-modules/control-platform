import struct
import logging

class Packager():
	"""
	Generates a bytestream for transmission of data. It gets the configuration bytes and the raw data to send.

	The raw data can be:
		single values of int or float
		lists of int or float (or both)
	"""

	def __init__(self):
		super().__init__()

	def __singleToByteArray(self, x, length):
		"""
		Converts a single value of float or int to its representation in bytes.
		"""
		if isinstance(x, float):
			return bytearray(struct.pack('<d', x))
		elif isinstance(x, int):
			return bytearray(x.to_bytes(length, 'little'))

	def __toByteArray(self, x, length):
		"""
		Converts a value or list to a bytestream.
		"""
		if isinstance(x, list):
			listByteArray = bytearray()
			for element in x:
				listByteArray += self.__singleToByteArray(element, length)
			return listByteArray
		else:
			return self.__singleToByteArray(x, length)

	def serializeConfig(self, conf, inputs = None, length = None):
		"""
		Converts a configuration input to a stream of bytes
		"""
		if length is None:
			length = 0

		confBytes = bytearray(conf)
		if inputs is not None: # isinstance(inputs, float) or isinstance(inputs, int) or inputs:
			valueBytes = self.__toByteArray(inputs, length)
			return confBytes+valueBytes
		else:
			return confBytes

	def serializeConfigBin(self, conf, inputs):
		"""
		Converts a configuration input, together with a binary payload input to a stream of bytes
		"""
		confBytes = bytearray(conf) + bytearray(inputs)
		return confBytes

class Interface():
	"""
	Interface prototype for extension with special interface functions
	"""
	def __init__(self, functionality):
		super().__init__()

		self.functionality = functionality
		self.packager = Packager()
	
	def send(self, keys, *args):
		"""
		Generic sending function
		"""
		command = self.functionality.keyList.walkKeychain(keys)
		return self.packager.serializeConfig(command, *args)

	def sendBinary(self, keys, binary):
		command = self.functionality.keyList.walkKeychain(keys)
		return self.packager.serializeConfigBin(command, binary)

	def sendDouble(self, keys, value):
		"""
		Send the platform keys field with a float value
		"""
		return self.send(keys, float(value), 8)

	def sendInt(self, keys, value):
		"""
		Send the platform keys field with an integer value
		"""
		return self.send(keys, int(value), 4)

class __Extension__():
	"""
	Base class for different platform modes
	"""
	def __init__(self, child, platform, configHandler, extName, debug = None):
		# Connect variables
		self.child = child
		self.platform = platform
		self.name = extName
		self.functionality = self.platform.functionality
		self.configHandler = configHandler

		self.interface = Interface(self.functionality)

		# Setup logging
		if debug is None:
			self.logger = logging.getLogger("general_logs.net.Extension")
		else:
			self.logger = logging.getLogger("general_logs.net.Extension@%s" % debug)

		self.logger.setLevel(logging.DEBUG)
